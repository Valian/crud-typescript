CREATE DATABASE fazt;

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(10),
    email TEXT
)

INSERT INTO users(name, email)
    VALUES ('joe', 'joe@saicoma.cl'),
            ('ryan', 'ryan@saicoma.cl');