import { Pool } from 'pg'

export const pool = new Pool({
    user        : 'postgres',
    host        : 'localhost',
    password    : 'asd',
    database    : 'fazt',
    port        : 5432
})